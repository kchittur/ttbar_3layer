
# coding: utf-8

# In[1]:

import pandas as pd
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt


# In[12]:

df = []
for i in range(80,81):
    for j in range(80,81):
        for k in range(80,81):
            in_str = str(i) + '_' + str(j) + '_' + str(k) +'.dat'
            mod = pd.read_csv('ani_dat/model_'+in_str,names=['Actual','Predicted'],skiprows=1)
            ind = [i,j,k]
            df.append([mod,ind])


# In[15]:

def pr(df):
    x = df['Actual']
    y = df['Predicted']
    x_m = np.mean(x)
    y_m = np.mean(y)
    numer = np.sum((x-x_m)*(y-y_m))
    den1 = np.sqrt(np.sum((x-x_m)**2))
    den2 = np.sqrt(np.sum((y-y_m)**2))
    denom = den1*den2
    return numer/denom


# In[20]:

for frame in df:
    mod = frame[0]
    ind = frame[1]
    i = ind[0]
    j = ind[1]
    k = ind[2]
    err_str = 'r=' + str(pr(mod))
    p = sns.lmplot(x='Actual',y='Predicted',data=mod,fit_reg=False)
    plt.plot([0, 500000], [0, 500000], linewidth=2,color='black')
    p.fig.text(0.24, 0.85,err_str, fontsize=12,fontweight='bold')
    plt.title('Layer 1: ' + str(i)+", Layer 2: " + str(j) + ', Layer 3: '+str(k))
    file_str = 'lineplots/model_'+str(i)+'_'+str(j)+'_'+str(k)+'.png'
    p.savefig(file_str,dpi=200)


# In[ ]:



