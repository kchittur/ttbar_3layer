
# coding: utf-8

# In[1]:

import pandas as pd
from matplotlib import pyplot as plt
#get_ipython().magic(u'matplotlib inline')
import numpy as np


# In[2]:

df = pd.read_csv('k_fold.dat',delimiter=' ',usecols=[0,1,2],names=['Mean','Std. Dev.','Iter'])
df['Mean']=np.log(df['Mean'])


# In[3]:

df.head()


# In[4]:

upper,lower = np.vsplit(df,[10])


# In[5]:

fal = []
for i in range(0,10):
    for j in range(i,9):
        fal.append([i,j+1])
fal = np.asarray(fal)


# In[6]:

mat=np.zeros((10,10));
mat[fal[:,0],fal[:,1]]=np.nan


# In[7]:

np.place(mat,mat==0,lower['Mean'])
mat=np.transpose(mat)


# In[8]:

up_dat = upper['Mean']
fin = np.concatenate([up_dat[np.newaxis,:],mat])


# In[9]:

import seaborn as sns


# In[14]:

ax=sns.heatmap(fin,cmap='inferno')
ax.invert_yaxis()

ax.set_xticklabels(range(1,11))
ax.set_xlabel('# nodes in layer 1',fontsize=15)
ax.set_ylabel('# nodes in layer 2',fontsize=15)
ax.set_title('Heatmap using LeakyReLU',fontsize=15)

cbar = ax.collections[0].colorbar
cbar.set_label(label='Log of Cost',size=14)


ax.figure.savefig('heatmap_400.png',dpi=1000)


# In[ ]:



