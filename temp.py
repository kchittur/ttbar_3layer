import ROOT
import math


f = ROOT.TFile('/data/kartik/410009.root')
t = f.Get("nominal")
#t.MakeClass("temp")


fl = open("410009.dat","w")

i = 0

for entry in t:
    if(entry.dilep_type > 0 or entry.trilep_type > 0):
        pt0 = entry.lep_Pt_0
        pt1 = entry.lep_Pt_1
        eta0 = entry.lep_Eta_0
        eta1 = entry.lep_Eta_1
        phi0 = entry.lep_Phi_0
        phi1 = entry.lep_Phi_1
        e0 = entry.lep_E_0
        e1 = entry.lep_E_1

        px0 = pt0 * math.cos(phi0)
        px1 = pt1 * math.cos(phi1)
        py0 = pt0 * math.sin(phi0)
        py1 = pt1 * math.sin(phi1)
        pz0 = pt0 * math.sinh(eta0)
        pz1 = pt1 * math.sinh(eta1)

        ptz = math.sqrt(math.pow(px0+px1,2)+math.pow(py0+py1,2))

        fl.write("%f %f %f %f %f %f %f\n" % (entry.Mll01,px0,px1,py0,py1,pz0,pz1))

        i+=1

        if i > 50000:
            break




fl.close()
