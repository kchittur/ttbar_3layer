import numpy as np
import pandas
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers.advanced_activations import LeakyReLU
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score, cross_val_predict
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from keras import backend as K
from keras.callbacks import ModelCheckpoint
import sys


def give_model(n_hidden1,n_hidden2,n_hidden3):
    def deep_model():
        fpath = 'weights_r/model_' + str(n_hidden1) + '_' + str(n_hidden2) + '_' + str(n_hidden3) + '.h5'
        model = Sequential([
        Dense(n_hidden1, input_dim=6, init='normal', name='dense_a')
        ])

        model.add(LeakyReLU(alpha=0.01))

        model.add(Dense(n_hidden2,init='normal',name='dense_b'))
        model.add(LeakyReLU(alpha=0.01))


        model.add(Dense(n_hidden3,init='normal',name='dense_c'))
        model.add(LeakyReLU(alpha=0.01))

        model.add(Dense(1, init = 'normal',name='dense_d'))

        model.load_weights(fpath,by_name=True)

        model.compile(loss = 'mean_squared_error', optimizer='adam')
        return model

    return deep_model



dataframe = pandas.read_csv(sys.argv[1], header=None,delimiter=' ')
dataset = dataframe.values
X = dataset[10000:20000,1:]
Y = dataset[10000:20000,0]

seed = 7
np.random.seed(seed)

mult_models = []
for i in range(80,81):
    for j in range(80,81):
        for k in range(80,81):
            mod = give_model(i,j,k)            
            ind = [i,j,k]
            mult_models.append([mod,ind])



for func in mult_models:
    mod = func[0]
    ind = func[1]

    fname = 'ani_dat/model_' + str(ind[0]) + '_' + str(ind[1]) + '_' + str(ind[2]) + '.dat'
    #reg = KerasRegressor(build_fn=mod, nb_epoch=200, batch_size=40, verbose=1)


    pred = mod().predict(X,batch_size=32,verbose=1)
    pred = np.concatenate([Y[:,np.newaxis],pred],axis=1)

    np.savetxt(fname,pred,delimiter=',',header='Actual,Pred',comments='')

