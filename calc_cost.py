import pandas as pd
import numpy as np

out_file = open('cost_3.dat','w')

for i in range(1,11):
    for j in range(1,i+1):
        mse = 0.0
        for k in range(1,j+1):
            mod_path = str(i) + '_' + str(j) + '_' + str(k) + '.dat'
            df = pd.read_csv('ani_dat/model_' + mod_path, delimiter = ',', names = ["Actual","Predicted"], skiprows=1)
            diff_sq = np.square(df["Actual"] - df["Predicted"])
            length = float(diff_sq.size)
            mse = np.sum(diff_sq)/length
            if k == 1:
                min_cost = mse
            if (k != 1) and ( mse < min_cost):
                min_cost = mse
        out_file.write("%d %d %f\n" % (i,j,min_cost))

out_file.close()  
            
