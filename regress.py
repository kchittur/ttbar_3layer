import numpy
import pandas
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers.advanced_activations import LeakyReLU
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score, cross_val_predict
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from keras import backend as K
import sys


def give_model(bool_layer2,n_hidden1,n_hidden2):
    def deep_model():

        model = Sequential([
        Dense(n_hidden1, input_dim=6, init='normal'),
        ])

        model.add(LeakyReLU(alpha=0.01))
        if bool_layer2:
            model.add(Dense(n_hidden2, init='normal'))
            model.add(LeakyReLU(alpha=0.01))
        model.add(Dense(1, init = 'normal'))

        model.compile(loss = 'mean_squared_error', optimizer='adam')
        return model

    return deep_model



dataframe = pandas.read_csv(sys.argv[1], header=None,delimiter=' ')
dataset = dataframe.values
X = dataset[:10000,1:]
Y = dataset[:10000,0]

seed = 7
numpy.random.seed(seed)

mult_models = []
for i in range(1,11):
    mult_models.append(give_model(False,i,0))



for i in range(1,11):
    for j in range(1,i+1):
        mult_models.append(give_model(True,i,j))


res = open("k_fold.dat","w")

z = 1
for func in mult_models:
    estimators = []
    #estimators.append(('standardize', StandardScaler()))
    estimators.append(('mlp', KerasRegressor(build_fn=func, nb_epoch=400, 
                                         batch_size=40, verbose=1)))
    print 'constructed pipeline\n'
    pipeline = Pipeline(estimators)


    kfold = KFold(n_splits=3, random_state=seed)
    results = cross_val_score(pipeline, X, Y, cv=kfold)
    res.write("%.4f %.4f %d\n" % (results.mean(), results.std(),z))
    z+=1

res.close()
