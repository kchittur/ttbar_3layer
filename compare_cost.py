
# coding: utf-8

# In[1]:

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
#get_ipython().magic(u'matplotlib inline')
import seaborn as sns


# In[15]:

df_2 = pd.read_csv("cost_2.dat",delimiter = ' ', names = ["i","j","mse"])


# In[16]:

df_3 = pd.read_csv("cost_3.dat",delimiter = ' ', names = ["i","j","mse"])


# In[20]:

df_diff = df_2.copy()
df_diff["mse"] = df_diff["mse"].subtract(df_3["mse"])
print(np.sum(df_diff["mse"]/float(55)))
