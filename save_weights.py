import numpy
import pandas
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers.advanced_activations import LeakyReLU
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score, cross_val_predict
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from keras import backend as K
from keras.callbacks import ModelCheckpoint
import sys


def give_model(n_hidden1,n_hidden2,n_hidden3):
    def deep_model():
        model = Sequential([
        Dense(n_hidden1, input_dim=6, init='normal', name='dense_a')
        ])

        model.add(LeakyReLU(alpha=0.01))

        model.add(Dense(n_hidden2,init='normal',name='dense_b'))
        model.add(LeakyReLU(alpha=0.01))

        model.add(Dense(n_hidden3,init='normal',name='dense_c'))
        model.add(LeakyReLU(alpha=0.01))

        model.add(Dense(1, init = 'normal',name='dense_d'))

        model.compile(loss = 'mean_squared_error', optimizer='Nadam')
        return model

    return deep_model



dataframe = pandas.read_csv(sys.argv[1], header=None,delimiter=' ')
dataset = dataframe.values
X = dataset[:10000,1:]
Y = dataset[:10000,0]

seed = 7
numpy.random.seed(seed)

mult_models = []
for i in range(80,81):
    for j in range(80,81):
        for k in range(80,81):
            mod = give_model(i,j,k)
            ind = [i,j,k]
            mult_models.append([mod,ind])

for func in mult_models:
    mod = func[0]
    ind = func[1]

    fpath = 'weights_r/model_' + str(ind[0]) + '_' + str(ind[1]) + '_' + str(ind[2]) + '.h5'

    reg = KerasRegressor(build_fn=mod, nb_epoch=3000, batch_size=40, verbose=1)

    save_call = ModelCheckpoint(fpath,monitor= 'val_loss',verbose = 0, save_best_only=False,save_weights_only=True,mode='auto')

    reg.fit(X,Y,callbacks=[save_call])

